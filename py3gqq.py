#!/usr/bin/env python
# coding: utf-8

import re
import requests
from bs4 import BeautifulSoup

class QQ:
    def __init__(self, qq, pwd, online=True):
        self.request = requests.Session()
        self.request.headers['User-Agent'] = 'rebot'
        self.login(qq, pwd, online)

    def login(self, qq, pwd, online):
        wml = self.request.post(
            'http://pt.3g.qq.com/psw3gqqLogin',
            data={
                'qq': qq,
                'pwd': pwd,
                'toQQchat': 'true',
                'loginType': int(online)}).content
        self.loginHandle(wml)

    def loginHandle(self, wml):
        r = BeautifulSoup(wml).card
        if r['title'] == '登录成功':
            sid = re.search(
                '(sid=)(.*?)(&)', r['ontimer'], re.S).group(2)
            self.baseUrl = 'http://q32.3g.qq.com/g/s?sid=' + sid
        else:
            raise Exception(r.findAll('p')[1].string.strip())

    def send(self, qq, msg):
        self.request.post(
            self.baseUrl, data={'u': qq, 'msg': msg, 'aid': '发送'})

    def getUserInfo(self, qq):
        wml = self.request.post(
            self.baseUrl,
            params={'aid': 'findnqqUserInfo'},
            data={'u': qq}).content
        return re.search(
            '(昵称:)(.*?)(\(|图标)', wml, re.S).group(2)

if __name__ == '__main__':
	import sys
	QQ('qq number', 'password').send(sys.argv[1], sys.argv[2])
